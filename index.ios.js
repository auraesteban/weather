var React = require('react-native');
var {
  AppRegistry,
  MapView,
  View,
  Text,
  StyleSheet
} = React;

var Api = require('./src/api.js');

var Weather = React.createClass({
	getInitialState: function() {
		return {
			pin: {
				latitude: 0,
				longitude: 0
			},
			city: '',
			temperature: '',
			description: ''
		}
	},
  	render: function() {
		return <View style={styles.container}>
			<MapView 
				annotations={[this.state.pin]}
		 		onRegionChangeComplete={this.onRegionChangeComplete} 
				style={styles.map}></MapView>
			<View style={styles.textWrapper}>
				<Text style={styles.text}>
					{this.state.city}
				</Text>
				<Text style={styles.text}>
					{this.state.temperature}
				</Text>
				<Text style={styles.text}>
					{this.state.description}
				</Text>
			</View>
		</View>
  	},
  	onRegionChangeComplete: function(region) {
  		this.setState({
  			pin: {
  				latitude: region.latitude,
  				longitude: region.longitude
  			}
  		});
  		// fat arrow function will make this === component
  		Api(region.latitude, region.longitude)
  			.then((data) => {
  				console.log(data);
  				this.setState(data); // Result will be added to the state objects i.e. city, temperature, description
  			});
  	}
});

var styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'stretch',
		backgroundColor: '#F5FCFF'
	},
  	map: {
		flex: 2,
		marginTop: 30
  	},
  	textWrapper: {
  		flex: 1,
  		alignItems: 'center',
  	},
  	text: {
  		fontSize: 30
  	}
});

AppRegistry.registerComponent('Weather', () => Weather);